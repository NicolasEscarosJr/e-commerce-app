// ProductCatalog.js
import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function ProductCatalog() {
  const [products, setProducts] = useState([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    fetch(`https://capstone-2-escaros.onrender.com/products/active`)
      .then(res => res.json())
      .then(data => {
        console.log(data); // Log the response data
        setProducts(data);
      })
      .catch(error => {
        console.error(error); // Log any error that occurred
      });
  }, []);

  return (
    <Container className="mt-5">
      <h2>Product Catalog</h2>
      <Row>
        {products.map(product => (
          <Col key={product._id} md={4}>
            <ProductCard product={product} user={user} />
          </Col>
        ))}
      </Row>
    </Container>
  );
}


