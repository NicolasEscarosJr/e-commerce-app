// Register.js
import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault();

    fetch("https://capstone-2-escaros.onrender.com/users/checkEmail", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then(res => res.json())
      .then(data => {
        if (data === true) {
          Swal.fire({
            title: 'Duplicate email found',
            icon: 'error',
            text: 'Kindly provide another email to complete the registration.',
          });
        } else {
          fetch("https://capstone-2-escaros.onrender.com/users/register", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              password: password1,
            }),
          })
            .then(res => res.json())
            .then(data => {
              if (data === true) {
                setFirstName('');
                setLastName('');
                setEmail('');
                setMobileNo('');
                setPassword1('');
                setPassword2('');

                Swal.fire({
                  title: 'Registration successful',
                  icon: 'success',
                  text: 'Welcome to our E-Commerce Store!',
                });

                navigate('/login');
              } else {
                Swal.fire({
                  title: 'Something went wrong',
                  icon: 'error',
                  text: 'Please try again.',
                });
              }
            });
        }
      });

    setFirstName('');
     setLastName('');
     setEmail('');
     setMobileNo('');
     setPassword1('');
     setPassword2('');
   }

   useEffect(() => {
     setIsActive(
       firstName !== '' &&
       lastName !== '' &&
       email !== '' &&
       mobileNo.length === 11 &&
       password1 !== '' &&
       password2 !== '' &&
       password1 === password2
     );
   }, [firstName, lastName, email, mobileNo, password1, password2]);

   return (
     <>
       {user ? (
         <Navigate to="/products" />
       ) : (
         <Form onSubmit={registerUser}>
           <Form.Group controlId="firstName">
             <Form.Label>First Name</Form.Label>
             <Form.Control
               type="text"
               placeholder="Enter first name"
               value={firstName}
               onChange={e => setFirstName(e.target.value)}
               required
             />
           </Form.Group>

          <Form.Group controlId="lastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter last name"
              value={lastName}
              onChange={e => setLastName(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="userEmail">
            <Form.Label>Email Address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter an email"
              value={email}
              onChange={e => setEmail(e.target.value)}
              required
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="mobileNo">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter Mobile Number"
              value={mobileNo}
              onChange={e => setMobileNo(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter a password"
              value={password1}
              onChange={e => setPassword1(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password2">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Confirm your password"
              value={password2}
              onChange={e => setPassword2(e.target.value)}
              required
            />
          </Form.Group>

          <Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled={!isActive}>
            Register
          </Button>
        </Form>
      )}
    </>
  );
}