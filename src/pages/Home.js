// Home.js
import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
  const data = {
    title: "Welcome to our E-Commerce Store",
    content: "Discover a wide range of products and enjoy a seamless shopping experience.",
    destination: "/product-catalog",
    label: "Shop Now"
  };

  return (
    <>
      <Banner data={data} />
      <Highlights />
    </>
  );
}
