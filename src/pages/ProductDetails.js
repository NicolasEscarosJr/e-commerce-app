// ProductDetails.js
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductDetails() {

  // The "useParams" hook allows us to retrieve the productId passed via the URL
  const { productId } = useParams();

  const { user } = useContext(UserContext);

  const userState = user && user.isAdmin;
  console.log(userState);

  // Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);
  const [totalAmount, setTotalAmount] = useState(0);

  const checkout = (productId) => {
    // Calculate the total amount based on the quantity and price
    const calculatedAmount = price * quantity;

    fetch('https://capstone-2-escaros.onrender.com/users/checkout', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity,
        amount: calculatedAmount
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if (!data === false) {
        Swal.fire({
          icon: 'success',
          title: 'Successfully checked out',
          text: "You have successfully checked out this product."
        });

        navigate("/product-catalog");
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Something went wrong',
          text: "Please try again."
        });
      }
    });
  };

  useEffect(() => {
    fetch(`https://capstone-2-escaros.onrender.com/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setTotalAmount(data.price); // Initialize the total amount with the product's price
      });
  }, [productId]);

  const handleQuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value, 10);
    setQuantity(newQuantity);

    // Recalculate the total amount based on the new quantity
    const calculatedAmount = price * newQuantity;
    setTotalAmount(calculatedAmount);
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title className="bg-dark text-white p-3">{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>Php {price}</Card.Text>
              <Form.Group>
                <Form.Label>Quantity:</Form.Label>
                <Form.Control
                  type="number"
                  value={quantity}
                  onChange={handleQuantityChange}
                  className="text-center"
                  placeholder="Enter quantity"
                  style={{ fontSize: '0.8rem' }} // Adjust the font size of the placeholder
                />
              </Form.Group>
              <Card.Subtitle>Total Amount:</Card.Subtitle>
              <Card.Text>Php {totalAmount}</Card.Text>
              {userState === false ? (
                <Button variant="primary" block onClick={() => checkout(productId)}>
                  Checkout
                </Button>
              ) : userState === true ? (
                <Link className="btn btn-warning btn-block" to="/login">
                  This Account is not Allowed to checkout
                </Link>
              ) : (
                <Link className="btn btn-danger btn-block" to="/login">
                  Log in to checkout
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}