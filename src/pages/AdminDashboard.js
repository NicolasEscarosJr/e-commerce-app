// AdminDashboard.js
import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Button, Spinner } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import AdminProductCard from '../components/AdminProductCard';
import CreateProductForm from '../components/CreateProductForm';
import UpdateProductForm from '../components/UpdateProductForm';
import Swal from 'sweetalert2';

const AdminDashboard = () => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [showCreateForm, setShowCreateForm] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) {
      navigate('/login');
      return;
    }

    fetch(`https://capstone-2-escaros.onrender.com/products`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setError('An error occurred. Please try again later.');
        setLoading(false);
      });
  }, []);

  const deactivateProduct = (productId) => {
    setLoading(true);
    fetch(`https://capstone-2-escaros.onrender.com/products/archive/${productId}`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        const updatedProducts = products.map((product) => {
          if (product._id === productId) {
            return { ...product, isActive: false };
          }
          return product;
        });
        setProducts(updatedProducts);
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setError('An error occurred. Please try again later.');
        setLoading(false);
      });
  };

  const reactivateProduct = (productId) => {
    setLoading(true);
    fetch(`https://capstone-2-escaros.onrender.com/products/activate/${productId}`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        const updatedProducts = products.map((product) => {
          if (product._id === productId) {
            return { ...product, isActive: true };
          }
          return product;
        });
        setProducts(updatedProducts);
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setError('An error occurred. Please try again later.');
        setLoading(false);
      });
  };

  const handleCreateProduct = (productData) => {
    setLoading(true);
    const token = localStorage.getItem('token');
    
    fetch(`https://capstone-2-escaros.onrender.com/products`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(productData),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        Swal.fire('Product Successfully Created');

        setProducts([...products, data]);
        setLoading(false);
        setShowCreateForm(false);
      })
      .catch((error) => {
        console.error(error);
        setError('An error occurred. Please try again later.');
        setLoading(false);
      });
  };

  const handleUpdateProduct = (productId, updatedData) => {
    setLoading(true);
    const token = localStorage.getItem('token');

    fetch(`https://capstone-2-escaros.onrender.com/products/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(updatedData),
    })
      .then((res) => {
        if (!res.ok) {
          throw new Error('Failed to update the product.');
        }
        return res.json();
      })
      .then((data) => {
        console.log(data);
        Swal.fire('Product Successfully Updated');

        const updatedProducts = products.map((product) => {
          if (product._id === productId) {
            return { ...product, ...updatedData };
          }
          return product;
        });
        setProducts(updatedProducts);
        setLoading(false);
        setSelectedProduct(null);
      })
      .catch((error) => {
        console.error(error);
        setError('An error occurred while updating the product. Please try again later.');
        setLoading(false);
      });
  };

  const toggleCreateForm = () => {
    setShowCreateForm(!showCreateForm);
  };

  const toggleUpdateForm = (product) => {
    setSelectedProduct(product);
  };

  const isAdmin = localStorage.getItem('token');
  if (!isAdmin) {
    return (
      <Container className="mt-5">
        <h2>Access Denied</h2>
        <p>You do not have permission to access the admin dashboard.</p>
      </Container>
    );
  }

  if (loading) {
    return (
      <Container className="mt-5">
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      </Container>
    );
  }

  if (error) {
    return <p>Error: {error}</p>;
  }

  if (!Array.isArray(products) || products.length === 0) {
    return <p>No products found.</p>;
  }

  return (
    <Container className="mt-5">
      <h2>Admin Dashboard</h2>
      <Row>
        <Col>
          <Button variant="primary" onClick={toggleCreateForm}>
            {showCreateForm ? 'Cancel' : 'Create Product'}
          </Button>
        </Col>
      </Row>
      {showCreateForm && (
        <Row className="mt-3">
          <Col>
            <CreateProductForm handleCreateProduct={handleCreateProduct} />
          </Col>
        </Row>
      )}
      <Row className="mt-3">
        {products.map((product) => (
          <Col key={product?._id} md={4}>
            <AdminProductCard
              product={product}
              onDeactivate={deactivateProduct}
              onReactivate={reactivateProduct}
              onUpdate={() => toggleUpdateForm(product)}
              loading={loading}
            />
          </Col>
        ))}
      </Row>
      {selectedProduct && (
        <Row className="mt-3">
          <Col>
            <UpdateProductForm
              product={selectedProduct}
              handleUpdateProduct={handleUpdateProduct}
            />
          </Col>
        </Row>
      )}
    </Container>
  );
};

export default AdminDashboard;
