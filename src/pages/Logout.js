// Logout.js
import { useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {
  const { unsetUser } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    unsetUser();
    navigate('/login');
  }, [unsetUser, navigate]);

  return null;
}
