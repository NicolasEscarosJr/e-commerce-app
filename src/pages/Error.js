// ErrorPage.js

import { Link } from 'react-router-dom';
import Banner from '../components/Banner';

export default function ErrorPage() {
  return (
    <>
      <Banner
        title="Page Not Found"
        subtitle="Go back to the "
        homepageText="homepage."
        isButton={false}
      />
    </>
  );
}
