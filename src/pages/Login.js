// Login.js
import React, { useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {
  const { setUser } = useContext(UserContext);
  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  function handleLogin(e) {
    e.preventDefault();

    fetch('https://capstone-2-escaros.onrender.com/users/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then(res => res.json())
      .then(data => {
        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);
          console.log(data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
                  icon: 'success',
                  title: 'Login successful!',
                  text: 'Welcome to my Phone shop',
                })

        } else {
          Swal.fire({
            icon: 'error',
            title: 'Authentication failed',
            text: 'Please check your login credentials and try again.',
          });
        }
      })
      .catch(error => {
        Swal.fire({
          icon: 'error',
          title: 'Authentication failed',
          text: 'An error occurred while logging in. Please try again.',
        });
        console.error('Login error:', error);
      })
      .finally(() => {
        setEmail('');
        setPassword('');
      });
  }

  const retrieveUserDetails = (token) => {
    fetch("https://capstone-2-escaros.onrender.com/users/details", {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.json())
      .then(data => {
        setUser({
          id: data._id,
          isAdmin: data.success

        });
        console.log(data)
        navigateAfterLogin(data.success); // Redirect based on user's role
      })
      .catch(error => {
        console.log("Error retrieving user details:", error);
      });
  };

  const navigateAfterLogin = (isAdmin) => {
    console.log(isAdmin)
    if (isAdmin) {
      navigate('/dashboard'); // Redirect to /dashboard for admin users
    } else {
      navigate('/product-catalog'); // Redirect to product catalog for non-admin users
    }
  };

  React.useEffect(() => {
    setIsActive(email !== '' && password !== '');
  }, [email, password]);

  return (
    <Form onSubmit={handleLogin}>
      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        />
      </Form.Group>

      <Button
        variant="primary"
        type="submit"
        id="submitBtn"
        className="mt-2"
        disabled={!isActive}
      >
        Login
      </Button>
    </Form>
  );
}

