// AppNavbar.js
import React, { useContext } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function AppNavbar() {
  const { user, unsetUser } = useContext(UserContext);
  const navigate = useNavigate();

  const handleLogout = () => {
    unsetUser();
    navigate('/login'); // Redirect to the login page after logout
  };

  return (
    <Navbar bg="cardHighlight" text="white" expand="lg"class="d-inline-block border-bottom border-danger">
      <Navbar.Brand as={NavLink} to="/">
        E-Commerce App
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">
          <Nav.Link as={NavLink} to="/" exact>
            Home
          </Nav.Link>
          {user && user.isAdmin && (
            <Nav.Link as={NavLink} to="/dashboard">
              Admin Dashboard
            </Nav.Link>
          )}
          {!user || !user.isAdmin ? (
            <Nav.Link as={NavLink} to="/product-catalog">
              Product Catalog
            </Nav.Link>
          ) : null}
          {user ? (
            <Nav.Link as={NavLink} to="/logout" onClick={handleLogout}>
              Logout
            </Nav.Link>
          ) : (
            <>
              <Nav.Link as={NavLink} to="/login">
                Login
              </Nav.Link>
              <Nav.Link as={NavLink} to="/register">
                Register
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
