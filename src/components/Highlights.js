// Highlights.js
import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
  return (
    <Row className="mt-3 mb-3">
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Shop from Home</h2>
            </Card.Title>
            <Card.Text>
              Discover a wide range of products from the comfort of your home. Browse through our extensive collection and find exactly what you need.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Flexible Payment Options</h2>
            </Card.Title>
            <Card.Text>
              Enjoy flexible payment options when you shop with us. We offer various payment methods to suit your convenience.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>
              <h2>Join Our Community</h2>
            </Card.Title>
            <Card.Text>
              Be a part of our community and stay updated with the latest trends, offers, and exclusive deals. Join now and unlock a world of possibilities.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
