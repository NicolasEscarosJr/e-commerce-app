// Banner.js
import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({ data }) {
  if (!data) {
    // Handle the case when data is undefined
    return null;
  }

  const { title, content, destination, label } = data;

  return (
    <Row>
      <Col>
        {title && <h1>{title}</h1>}
        {content && <p>{content}</p>}
        {destination && label && <Link to={destination}>{label}</Link>}
      </Col>
    </Row>
  );
}