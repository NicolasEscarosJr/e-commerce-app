// AdminProductCard.js
import React from 'react';
import { Card, Button } from 'react-bootstrap';

const AdminProductCard = ({ product, onDeactivate, onReactivate, onUpdate, loading }) => {
  const handleDeactivate = () => {
    onDeactivate(product._id);
  };

  const handleReactivate = () => {
    onReactivate(product._id);
  };

  return (
    <Card className="my-3">
      <Card.Body>
        <Card.Title>{product.name}</Card.Title>
        <Card.Text>{product.description}</Card.Text>
        <Card.Text>Price: {product.price}</Card.Text>
        {product.isActive ? (
          <Button variant="danger" onClick={handleDeactivate} disabled={loading}>
            Deactivate
          </Button>
        ) : (
          <Button variant="success" onClick={handleReactivate} disabled={loading}>
            Reactivate
          </Button>
        )}
        <span className="mx-1"></span> {/* Space between the buttons */}
        <Button variant="primary" onClick={onUpdate} disabled={loading}>
          Update
        </Button>
      </Card.Body>
    </Card>
  );
};

export default AdminProductCard;
