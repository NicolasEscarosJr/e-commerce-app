// App.js
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import ProductCatalog from './pages/ProductCatalog';
import ProductDetails from './pages/ProductDetails';
import UserContext, { UserProvider } from './UserContext';
import AdminDashboard from './pages/AdminDashboard';

import './App.css';

function App() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    if (localStorage.getItem('token')) {
      fetch("https://capstone-2-escaros.onrender.com/users/details", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(data => {
          if (data._id) {
            console.log(data); // Check the user object
            setUser(data);
          }
        })
        .catch(error => {
          console.log("Error retrieving user details:", error);
        });
    }
  }, []);

  const unsetUser = () => {
    localStorage.removeItem('token');
    setUser(null);
  };

 return (
     <UserProvider value={{ user, setUser, unsetUser }}>
       <Router>
         <AppNavbar />
         <Container>
           <Routes>
             <Route path="/" element={<Home />} />
             <Route path="/product-catalog" element={<ProductCatalog />} />
             <Route path="/products/:productId" element={<ProductDetails />} />
             <Route path="/login" element={<Login />} />
             <Route path="/logout" element={<Logout />} />
             <Route path="/register" element={<Register />} />
             <Route path="/dashboard" element={<AdminDashboard />} />
             <Route path="/*" element={<Error />} />
           </Routes>
         </Container>
       </Router>
     </UserProvider>
   );
 }

 export default App;